import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Figure kolo = new Kolo(10);
        Figure prostokat = new Prostokat(1, 5);
        Figure kwadrat = new Kwadrat();

        List figury = new ArrayList();
        figury.add(kolo);
        figury.add(prostokat);
        figury.add(kwadrat);
        figury.add(new Integer(1));

        for (int i = 0; i < figury.size(); i++) {
            if (figury.get(i) instanceof Figure) {
                System.out.println(
                        ((Figure) figury.get(i))
                                .obliczObwod());
                System.out.println(
                        ((Figure) figury.get(i))
                                .obliczPole());
            }
        }
    }
}
