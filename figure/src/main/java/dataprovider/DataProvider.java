package dataprovider;

interface DataProvider {
    int nextInt();
    String nextString();
}
