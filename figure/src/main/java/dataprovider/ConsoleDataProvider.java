package dataprovider;

import java.util.Scanner;

public class ConsoleDataProvider implements DataProvider {
    @Override
    public int nextInt() {
        System.out.print("Podaj wiek: ");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    @Override
    public String nextString() {
        System.out.print("Podaj imię i nazwisko: ");
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }
}
