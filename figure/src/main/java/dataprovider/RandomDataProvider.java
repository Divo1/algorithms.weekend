package dataprovider;

import java.util.*;

public class RandomDataProvider implements DataProvider {
    private Random rand = new Random();
    private List<String> imiona = new ArrayList<>();

    public RandomDataProvider() {
        imiona.add("Mariusz Kowalczyk");
        imiona.add("Jan Nowak");
        imiona.add("Jan Kowalski");
        imiona.add("Maria Jabłońska");

        Set<Integer> hset = new HashSet<>();
        Integer[] tablica = new Integer[] {1, 2,3};
        hset.addAll(Arrays.asList(tablica));

        for (int i = 0; i < tablica.length; i++) {
            hset.add(tablica[i]);
        }
    }

    @Override
    public int nextInt() {
        return rand.nextInt(100) + 1;
    }

    @Override
    public String nextString() {
        return imiona.get(rand.nextInt(imiona.size()));
    }
}
