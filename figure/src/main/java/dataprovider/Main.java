package dataprovider;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Jakiego providera użyć: ");
        int i = in.nextInt();
        DataProvider dp = null;
        if (i == 1) {
            dp = new StaticDataProvider();
        } else if (i == 2) {
            dp = new ConsoleDataProvider();
        } else if (i == 3) {
            dp = new RandomDataProvider();
        }

        if (dp != null) {
            int wiek = dp.nextInt();
            String name = dp.nextString();
            System.out.println("Wiek użytkownika: " + wiek);
            System.out.println("Imię i nazwisko: " + name);
        }
    }
}
