package town;

public class Main {
    public static void main(String[] args) {
        Town town = new Town();

        town.addCitizen(new Peasent("Tom"));
        town.addCitizen(new Peasent("Jerzy"));
        town.addCitizen(new Peasent("Seba"));

        System.out.println(town.howManyCanVote());
    }
}
