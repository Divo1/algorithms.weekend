public class Kwadrat implements Figure {
    @Override
    public double obliczPole() {
        return 4;
    }

    @Override
    public double obliczObwod() {
        return 4;
    }
}
