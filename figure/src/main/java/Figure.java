public interface Figure {
    double obliczPole();
    double obliczObwod();
}
